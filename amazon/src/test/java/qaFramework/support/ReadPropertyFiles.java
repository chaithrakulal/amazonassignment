package qaFramework.support;

import java.io.InputStream;
import java.util.Properties;

public class ReadPropertyFiles {

	public Properties ReadEnvironment() throws Exception {
		Properties environmentProps = new Properties();
		String propertiesFilename = System.getProperty("environment.file", "/propertiesFiles/environment.properties");
		InputStream is = this.getClass().getResourceAsStream(propertiesFilename);
		environmentProps.load(is);
		
		return environmentProps;
	}


}
