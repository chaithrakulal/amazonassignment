package pages;

import data.SharedData;
import io.appium.java_client.AppiumDriver;
import qaFramework.pageObject.MobilePageObject;

public class Checkout  extends MobilePageObject{
	private String SELECT_ITEM_PART1    = "//android.view.View[@text='",
			       DONE                 = "android.view.View[@text='DONE']";
	        		  
	public Checkout(AppiumDriver<?> driver) throws Exception {
		super(driver);
	}

	/** This function is to verify the Item Details **/

	public Checkout verifyDescription(String item) throws Exception {
		waitForElementIsVisible(SELECT_ITEM_PART1 + item + SharedData.CLOSE_XPATH, "xpath");
		verifyText(SELECT_ITEM_PART1 + item + SharedData.CLOSE_XPATH, "xpath", item, "");
		return this;

	}

	/** This function is to verify the Price Details **/
	public Checkout verifyPrice(String price) throws Exception {
		waitForElementIsVisible(SELECT_ITEM_PART1 + price + SharedData.CLOSE_XPATH, "xpath");
		verifyText(SELECT_ITEM_PART1 + price + SharedData.CLOSE_XPATH, "xpath", price, "");
		return this;
	}

	/** This function is to click done after checkout **/
	public Checkout clickDone() throws Exception {
		waitForElementIsVisible(DONE, "xpath");
		click(DONE, "xpath");
		return this;
	}

}
