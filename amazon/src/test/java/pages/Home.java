package pages;

import data.SharedData;
import io.appium.java_client.AppiumDriver;
import qaFramework.pageObject.MobilePageObject;

public class Home extends MobilePageObject{
	private String SEARCH                       = "rs_search_src_text",
			       SELECT_ITEM_PART1            = "//android.widget.TextView[@text='";			    

	
	public Home(AppiumDriver<?> driver) throws Exception {
		super(driver);
	}

	/** This function is to click search **/

	public Home clickSearch() throws Exception {
		waitForElementIsVisible(SEARCH, "id");
		click(SEARCH, "id");
		return this;

	}

	/** This function is to search item **/

	public Home searchItem(String item) throws Exception {
		waitForElementIsVisible(SEARCH, "id");
		click(SEARCH, "id");
		sendKeys(SEARCH, "id", item);
		return this;

	}

	/** This function is to select the searched item from the list **/
	public Home selectItem(String item) throws Exception {
		waitForElementIsVisible(SELECT_ITEM_PART1 + item + SharedData.CLOSE_XPATH_1, "xpath");
		click(SELECT_ITEM_PART1 + item + SharedData.CLOSE_XPATH_1, "xpath");
		return this;

	}

}
