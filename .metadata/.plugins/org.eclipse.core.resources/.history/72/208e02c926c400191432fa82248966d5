package configuration;


import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import qaFramework.support.BrowserOptions;
import qaFramework.support.ClearTemporaryFiles;
import qaFramework.support.DateAndTimeFunctions;
import qaFramework.support.Date_Time_settings;
import qaFramework.support.OfficeCommonFunctions;
import qaFramework.support.ReadPropertyFiles;


public class Configuration {

	public static WebDriver driver;
	public static String browser;
	public static String siteHost;
	public static String os;
	protected String TCID;
	protected String TC_DESCRIPTION;
	public String Result;
	public static AppiumDriver<MobileElement> driver2;
	BrowserOptions browserOptions = new BrowserOptions();
	public String module = "web";
	public String platformName = "Android";



	@BeforeMethod
	public void setUp() throws Exception {
		Date_Time_settings dts = new Date_Time_settings();
		siteHost =  System.getProperty("siteHost");
		browser = System.getProperty("browser");
		os      = System.getProperty("os");
		if ("web".equalsIgnoreCase(module)) {


			if ("chrome".equals(browser)) {
				getChromedriver();


			} if ("iexplore".equals(browser)) {			
				getIEdriver();

			}  if ("firefox".equals(browser)) {
				getFirefoxdriver();

			}
			//driver.manage().window().maximize();
			driver.get(siteHost);
		}
		else if ("mobile".equalsIgnoreCase(module)) {
			mobilesetUp();
		}

	}

	public WebDriver getIEdriver() throws Exception {
		InternetExplorerOptions options = new InternetExplorerOptions();
		if ("windows".equals(os)) {
			browserOptions.ieOptions(options);}
		return driver = new InternetExplorerDriver(options);

	}

	public WebDriver getChromedriver()throws Exception{
		ChromeOptions options = new ChromeOptions();
		if ("windows".equals(os)) {
			browserOptions.windowsChromeOptions(options);
		}
		if ("linux".equals(os)) {
			browserOptions.linuxChromeOptions(options);
		}
		return driver = new ChromeDriver(options);
	}
	public WebDriver getFirefoxdriver() throws Exception {
		FirefoxOptions options = new FirefoxOptions();
		if ("windows".equals(os)) {
			browserOptions.firefoxOptions(options);}
		return driver = new FirefoxDriver(options);
	}


	public void mobilesetUp() throws Exception {

		System.out.println(MobilePlatform.ANDROID);
		ReadPropertyFiles readPropertyFiles = new ReadPropertyFiles();
		Properties envPropertyDetails = readPropertyFiles.ReadEnvironment();

		if (MobilePlatform.IOS == platformName) {

			String appPackage = envPropertyDetails.getProperty("iOS_appPackage");
			String apkPath = System.getProperty("user.dir") + envPropertyDetails.getProperty("iOS_apkPath");
			String iOS_platformName = envPropertyDetails.getProperty("iOS_platformName");
			String platformVersion = envPropertyDetails.getProperty("iOS_platformVersion");
			String deviceName = envPropertyDetails.getProperty("iOS_deviceName");
			String udId = envPropertyDetails.getProperty("UDID");

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("platformName", iOS_platformName);
			capabilities.setCapability("platformVersion", platformVersion);
			capabilities.setCapability("deviceName", deviceName);
			capabilities.setCapability("udid", udId);
			capabilities.setCapability(MobileCapabilityType.APP, apkPath);
			capabilities.setCapability(MobileCapabilityType.APPLICATION_NAME, appPackage);
			capabilities.setCapability("noReset", true);
			capabilities.setCapability("fullReset", false);
			capabilities.setCapability("newCommandTimeout", "2000000");
			capabilities.setCapability("simpleIsVisibleCheck", true);
			capabilities.setCapability("automationName", "XCUITest");
			driver2 = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			driver2.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}
		else if (MobilePlatform.ANDROID == platformName) {
			String appPackage = envPropertyDetails.getProperty("appPackage");
			String apkPath = System.getProperty("user.dir") + envPropertyDetails.getProperty("apkPath");
			String deviceName = envPropertyDetails.getProperty("deviceName");

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("deviceName", deviceName);
			capabilities.setCapability("platformName", MobilePlatform.ANDROID);
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 1000000);
			capabilities.setCapability("appPackage", appPackage);
			capabilities.setCapability("appActivity", "com.intermedix.eics.activities.StartupActivity_");
			capabilities.setCapability("automationName", "UiAutomator2");
			capabilities.setCapability("app", apkPath);
			driver2 = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
			driver2.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}

	}

	@AfterMethod


	public void tearDown(ITestResult result) throws Exception {
		DateAndTimeFunctions dateAndTimeFunctions = new DateAndTimeFunctions();
		if(ITestResult.FAILURE==result.getStatus()) { try { TakesScreenshot ts =
				(TakesScreenshot)driver; File source=ts.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(source, new
						File("./src/test/resources/Screenshots/"+result.getName()+".png")); } catch
			(Exception e) {
							System.out.println("Exception while taking screenshot "+e.getMessage()); } }

		if (ITestResult.SKIP == result.getStatus()) {
			Result = "FAIL";
			FileWriter fw = new FileWriter(".\\src\\test\\resources\\FailResults.txt", true);
			PrintWriter out = new PrintWriter(fw);
			out.println(result.getName() + "," + Result + ","
					+ dateAndTimeFunctions.getCurrentDate("MM/dd/yyyy hh:mm"));
			out.close();
		} // code to write results to Notepad Since QNET is not available --FIXME
		if (ITestResult.SUCCESS == result.getStatus()) {
			Result = "PASS";
			FileWriter fw = new FileWriter(".\\src\\test\\resources\\PassResults.txt", true);
			PrintWriter out = new PrintWriter(fw);
			out.println(result.getName() + "," + Result + ","
					+ dateAndTimeFunctions.getCurrentDate("MM/dd/yyyy hh:mm"));
			out.close();
		}
		
		if (driver != null) {
			driver.quit();
		}
	}
		//browserOptions.killBrowserDriver(); }

	@AfterClass
	public void closeBrowser() {
		try {

			ClearTemporaryFiles clearTemporaryFiles = new ClearTemporaryFiles();
			//clearTemporaryFiles.deleteTempFiles();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}