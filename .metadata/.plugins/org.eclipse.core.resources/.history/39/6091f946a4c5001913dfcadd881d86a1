package qaFramework.pageObject;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import configuration.Configuration;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;


public class MobilePageObject extends Configuration{

	public static AppiumDriver<MobileElement> driver;
	public static final int WAIT_TIME_LONGDURATION = 100;
	public static final int WAIT_TIME_SHORTDURATION = 100;
	public static final int WAIT_TIME = 100;

	public MobilePageObject(AppiumDriver<?> driver2) throws Exception{
		MobilePageObject.driver =  Configuration.driver2;
	}

	public enum type {
		xpath, css, id, name, tagName, linktext,className;
	}

	public static By getWebDriverBy(String elementName, String locatorStrategy)
			throws Exception {
		By webdriverby = null;

		switch (type.valueOf(locatorStrategy)) {
		case xpath:
			webdriverby = By.xpath(elementName);
			break;
		case css:
			webdriverby = By.cssSelector(elementName);
			break;
		case id:
			webdriverby = By.id(elementName);
			break;
		case name: 
			webdriverby = By.name(elementName);
			break;
		case tagName:
			webdriverby = By.tagName(elementName);
			break;
		case linktext:
			webdriverby = By.linkText(elementName);
			break;
		case className:
			webdriverby = By.className(elementName);
		}
		return webdriverby;
	}
	public WebElement getElement(String elementName, String locatorStrategy)throws Exception{
		WebElement element = driver.findElement(getWebDriverBy(elementName, locatorStrategy));
		return element;
	}

	public List<MobileElement> getElementLists(String elementName, String locatorStrategy) throws Exception {
		return driver.findElements(getWebDriverBy(elementName, locatorStrategy));
	}
	/************** Selenium Functions*********************/

	public void sendKeys(String elementName, String locatorStrategy, String keys) throws Exception {
		waitForElementIsVisible(elementName, locatorStrategy);
		getElement(elementName, locatorStrategy).clear();
		getElement(elementName, locatorStrategy).sendKeys(keys);
	}

	public String getText(String elementName, String locatorStrategy) throws Exception {
		String text = getElement(elementName, locatorStrategy).getText();
		return text;
	}
	public String getAttributeValue(String elementName, String locatorStrategy) throws Exception {
		String text = getElement(elementName, locatorStrategy).getAttribute("value");
		return text;
	}

	/************** Scroll Functions*********************/

	//This function scroll the page till the element is found
	public void scrollToElement(String elementName, String locatorStrategy) throws Exception{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", getElement( elementName, locatorStrategy));
	}

	/************** Click Functions  *********************/

	public void click(String elementName, String locatorStrategy) throws Exception {
		waitForElementToBeClickable(elementName, locatorStrategy);
		getElement(elementName, locatorStrategy).click();
	}


	/**************Wait Functions *********************/
	//wait for text present 

	public boolean waitForTextPresent(String elementName, String locatorStrategy,String text) throws Exception{
		WebElement element = getElement( elementName, locatorStrategy);
		boolean condition = true;
		try{
			WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
			wait.until(ExpectedConditions.textToBePresentInElement(element, text));
		}
		catch (Exception ex){
			condition = false;
			throw ex;
		}
		return condition;
	}

	
	//Expectation for checking an element is visible and enabled
	public void waitForElementToBeClickable(String elementName, String locatorStrategy)throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
		wait.until(ExpectedConditions.elementToBeClickable(getElement(elementName,locatorStrategy)));
	}

	

	//An expectation for checking that an element, known to be present on the DOM of a page, is visible
	public WebElement waitForElementIsVisible(String elementName, String locatorStrategy)throws Exception{
		WebElement element = getElement(elementName,locatorStrategy);
		WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
		wait.until(ExpectedConditions.visibilityOf(element) );
		return element;
	}

	

	//An expectation for checking the element to be invisible
	public WebElement waitForInvisibleOfElement(String elementName, String locatorStrategy)throws Exception{
		WebElement element = getElement(elementName,locatorStrategy);
		WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
		wait.until(ExpectedConditions.invisibilityOf(element));
		return element;
	}

	//An expectation for checking the page load
	public static void waitForPageLoad() {
		try {
			ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver webdriver) {
					return ((JavascriptExecutor) webdriver).executeScript("return document.readyState")
							.equals("complete");
				}
			};
			WebDriverWait wait = new WebDriverWait(Configuration.driver,WAIT_TIME_LONGDURATION );
			wait.until(pageLoadCondition);
		} catch (WebDriverException we) {
			System.out.println(we.toString());
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}



	// Checks is element present returns boolean value
	public boolean isElementPresent(String elementName, String locatorStrategy) {
		try {
			if (driver.findElements(getWebDriverBy(elementName,locatorStrategy)).size()!= 0) {
				return true;

			} else {
				return false;
			}
		} catch (WebDriverException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	
	public void waitForElementAppearance(String elementName, String locatorStrategy) throws Exception {
		new WebDriverWait(driver, WAIT_TIME_LONGDURATION).until(ExpectedConditions.presenceOfElementLocated(getWebDriverBy(elementName,locatorStrategy)));
	}


	
	public  void verifyText(String elementName, String locatorStrategy, String strValue, String errorMessage) throws Exception {
		assertEquals(strValue, getElement(elementName,locatorStrategy).getText().trim(),errorMessage);
	}

	public  void verifyTextIsPresent(String elementName, String locatorStrategy, String errorMessage) throws Exception {
		assertTrue(isElementPresent(elementName,locatorStrategy),errorMessage);
	}

	
	public  void verifyTextIsDisplayed(String elementName, String locatorStrategy, String errorMessage) throws Exception {
		assertTrue(getElement(elementName,locatorStrategy).isDisplayed(),errorMessage);
	}
	
	
	/**************Common Appium Functions *********************/
	
	 public void waitForPageToLoad(WebElement id) {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.elementToBeClickable(id));
	    }
	 
	  

	    public boolean isElementPresent(By by) {
	        try {
	            driver.findElement(by);
	            return true;
	        } catch (NoSuchElementException e) {
	            return false;
	        }

	    }

	    public void swipeLeftUntilTextExists(String expected) {
	        do {
	            swipeLeft();
	        } while (!driver.getPageSource().contains(expected));
	    }

	    public void swipeRight() {
	        Dimension size = driver.manage().window().getSize();
	        int startx = (int) (size.width * 0.9);
	        int endx = (int) (size.width * 0.20);
	        int starty = size.height / 2;
	        new TouchAction(driver).press(PointOption.point(startx, starty))
	                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
	                .moveTo(PointOption.point(endx,starty)).release().perform();
	    }

	    public void swipeLeft() {
	        Dimension size = driver.manage().window().getSize();
	        int startx = (int) (size.width * 0.8);
	        int endx = (int) (size.width * 0.20);
	        int starty = size.height / 2;
	        new TouchAction(driver).press(PointOption.point(startx, starty))
	                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)))
	                .moveTo(PointOption.point(endx,starty)).release();
	    }


	    public void clickBackButton() {
	        driver.navigate().back(); //Closes keyboard
	    }


	    public void waitForPageToLoadMobile(WebElement id) {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.elementToBeClickable(id));
	    }
	    
	    public void waitForElementVisible(MobileElement id) {
	        WebDriverWait wait = new WebDriverWait(driver, 15);
	        wait.until(ExpectedConditions.visibilityOf(id));
	    }
}



